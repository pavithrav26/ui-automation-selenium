package testngautomationPackage;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import testngautomationPackage.PageFactory.UsbankHomePage;
import testngautomationPackage.PageFactory.LaunchWebPage;
import testngautomationPackage.PageFactory.UsLoginPage;

public class TestNGRunner {
	WebDriver driver;

	@BeforeMethod
	public void launchApp() throws Exception {
		
		LaunchWebPage lWebPage=new LaunchWebPage();	
		driver = lWebPage.launchWebPage("chrome","http://www.usbank.com");
	}
	/**
	 * This method navigates to the login page and also enter the credentials through the page and validate the same.
	 * @param userid
	 * @param pwd
	 * @throws Exception
	 */
	@Test(dataProvider = "User Credentials")
	public void testNavigationToLoginPage(String userid, String pwd) throws Exception {
		try
		{
		UsbankHomePage bbyPage = PageFactory.initElements(LaunchWebPage.driver, UsbankHomePage.class);
		bbyPage.clickMenuOnHomePage();
		UsLoginPage bbyLoginPage = PageFactory.initElements(LaunchWebPage.driver, UsLoginPage.class);
		bbyLoginPage.enterLoginPage(userid,pwd);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@AfterMethod
	public void driverClose() {
		if (driver != null) {
			driver.close();
		}
	}

	@AfterClass
	public void driverQuit() {
		if (driver != null) {
			driver.quit();
		}
	}
/**
 * This data provider provides data from the text file for different credentials
 * @return
 * @throws Exception
 */
	@DataProvider(name = "User Credentials")
	public Object[][] userCredentials() throws Exception {
		LaunchWebPage lWebPage=new LaunchWebPage();	
		return lWebPage.testData();
	}
}
