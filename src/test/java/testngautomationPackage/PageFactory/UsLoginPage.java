package testngautomationPackage.PageFactory;


import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import utils.TestUtils;

public class UsLoginPage {
	
	@FindBy(how=How.XPATH, using ="//input[@id='aw-personal-id']")
	WebElement userid;
	@FindBy(how=How.XPATH, using ="//input[@id='aw-password']")
	WebElement pwd;
	@FindBy(how=How.XPATH, using ="//input[@id='aw-remember-my-id']")
	WebElement remember;
	@FindBy(how=How.XPATH, using ="//button[@id='aw-log-in']")
	WebElement btnLogin;
	@FindBy(how=How.XPATH, using ="//div[@class='error-backdrop']/p[@class='ng-binding'][@aria-live='assertive']")
	WebElement errorMessage;
	
	Select bankingType;
	String bankingTypeXpath ="//select[@id='aw-account-type']";
	
	public void enterLoginPage(String useridValue,String pwdValue) throws Exception {
		try {
			WebDriver driver = LaunchWebPage.driver;
			
		TestUtils.textSendKeys(userid, useridValue);	
		TestUtils.textSendKeys(pwd, pwdValue);	

		TestUtils.webElementClick(remember);
		
		if(TestUtils.isElementPresent(driver, By.xpath(bankingTypeXpath))) {
			bankingType = new Select(driver.findElement(By.xpath(bankingTypeXpath)));
			bankingType.selectByVisibleText("Online Investing");
		}else {
			System.out.println("bankingType is not present.");
		}
		
		TestUtils.webElementClick(btnLogin);
		
		TestUtils.waitforVisibility(errorMessage, 2);
		
		assertEquals(errorMessage.getText(), "Personal ID is 7-22 characters, no spaces or special characters.");
		
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		
	}

}
