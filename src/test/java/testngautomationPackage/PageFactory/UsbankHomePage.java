package testngautomationPackage.PageFactory;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import utils.TestUtils;

public class UsbankHomePage {
	@FindBy(how = How.XPATH,using = "//div[@id='personal-sub-nav']//a/span[contains(text(),'Online & Mobile')]")
	WebElement onlineBanking;
	@FindBy(how=How.XPATH, using ="//div[@class='megaNavLinksInColumn']//a[text()='Online Banking']")
	WebElement onlineBankingMenu;
	@FindBy(how=How.XPATH, using ="//div[@id='closeChat'][@onclick='hideLightBox()']")
	WebElement closePopup;
	
	@FindBy(how=How.XPATH, using ="//p[@class='red-circular-bottom-zero']//a/span[text()='Log in']")
	WebElement loginLink;
	@FindBy(how=How.XPATH, using ="//select[@id='aw-account-type']")
	Select bankingType;
	@FindBy(how=How.XPATH, using ="//input[@id='aw-personal-id']")
	WebElement userid;
	@FindBy(how=How.XPATH, using ="//input[@id='aw-remember-my-id']")
	WebElement remember;
	int waitforTime;
	
	WebDriver driver = LaunchWebPage.driver;
	
	public void clickMenuOnHomePage() throws Exception {
		try {
			
		TestUtils.webElementClick(onlineBanking);
		
		TestUtils.webElementClick(onlineBankingMenu);
		
		TestUtils.webElementClickifDisplayed(closePopup);
		
		TestUtils.webElementClick(loginLink);
		
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		
	}
}
