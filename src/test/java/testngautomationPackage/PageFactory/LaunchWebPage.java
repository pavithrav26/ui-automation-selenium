package testngautomationPackage.PageFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver;

import testngautomationPackage.TestParam;

public class LaunchWebPage {

	public static WebDriver driver;

	public WebDriver launchWebPage(String browserType, String URL) {

		if (browserType.equalsIgnoreCase("chrome")) {
			driver = new ChromeDriver();
			driver.get(URL);

			driver.manage().window().maximize();

			driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
		} else {
			driver = new SafariDriver();
			driver.get(URL);

			driver.manage().window().maximize();

			driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
		}
		return driver;
	}

	public Object[][] testData() throws Exception {
		BufferedReader br;

		File file = new File("src/test/resources/text.txt");

		br = new BufferedReader(new FileReader(file));

		String st;
		List<String> fileRows=new ArrayList<String>();
		while ((st = br.readLine()) != null) {
			System.out.println(st);
			fileRows.add(st);
		}
		
		Object[][] finalObj = new Object[fileRows.size()][2];

		for (int i = 0; i < fileRows.size(); i++) {
			TestParam ts = new TestParam();

			String[] strValues = fileRows.get(i).toString().split(",");
			ts.setUserid(strValues[0]);
			ts.setPwd(strValues[1]);

			finalObj[i][0] = ts.getUserid();
			finalObj[i][1] = ts.getPwd();

		}
		br.close();

		return finalObj;

	}
}
