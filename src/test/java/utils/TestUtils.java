package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import testngautomationPackage.PageFactory.LaunchWebPage;

public class TestUtils {

	/**
	 * The method will verify if the element is present on the page without throwing
	 * an exception
	 * 
	 * @param driver
	 * @param by
	 * @return
	 */
	public static boolean isElementPresent(WebDriver driver, By by) {

		try {
			driver.findElement(by);
			return true;

		} catch (NoSuchElementException e) {
			return false;

		}
	}

	/**
	 * It uses Web driver wait for waiting for an element to become clickable on the
	 * web page
	 * 
	 * @param elementToWaitFor
	 * @param waitTime
	 * @return
	 * @throws Exception
	 */
	public static boolean waitforClickability(WebElement elementToWaitFor, int waitTime) throws Exception {
		WebElement element;

		WebDriverWait wait = new WebDriverWait(LaunchWebPage.driver, waitTime);
		try {
			element = wait.until(ExpectedConditions.elementToBeClickable(elementToWaitFor));
		} catch (Exception e) {
			throw new Exception(e);
		}

		if (element != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * The method waits for an element to be loaded on the web page
	 * 
	 * @param elementToWaitFor
	 * @param waitTime
	 * @return
	 * @throws Exception
	 */

	public static boolean waitforVisibility(WebElement elementToWaitFor, int waitTime) throws Exception {
		WebElement element;

		WebDriverWait wait = new WebDriverWait(LaunchWebPage.driver, waitTime);
		try {
			element = wait.until(ExpectedConditions.visibilityOf(elementToWaitFor));
		} catch (Exception e) {
			throw new Exception(e);
		}

		if (element != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * This utility method waits for the web element to load for 10 seconds and
	 * clicks on it
	 * 
	 * @param elementToClick
	 */
	public static void webElementClick(WebElement elementToClick) {

		try {
			if (waitforVisibility(elementToClick, 10)) {
				elementToClick.click();
			} else {
				System.out.println(elementToClick + "is not present on the webpage");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * This method is to type the text on an input box waiting it to load for 10 sec
	 * 
	 * @param elementToClick
	 * @param textToEnter
	 */
	public static void textSendKeys(WebElement elementToClick, String textToEnter) {

		try {
			if (waitforVisibility(elementToClick, 10)) {
				elementToClick.sendKeys(textToEnter);
			} else {
				System.out.println(elementToClick + "is not present on the webpage");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * The below method is to handle the pop-up the might appear in between the
	 * execution.
	 * 
	 * @param elementToClick
	 * @throws Exception
	 */

	public static void webElementClickifDisplayed(WebElement elementToClick) {

		try {
			if (waitforVisibility(elementToClick, 5)) {
				elementToClick.click();
			}
		} catch (Exception e) {
			// No code needed here as the above click is to handle the popup if present and
			// do nothing if not present

		}

	}

}
